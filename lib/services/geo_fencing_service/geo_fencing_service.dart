import 'package:easy_geofencing/easy_geofencing.dart';

class GeoFencingService {
  GeoFencingService(double latitude, double longitude) {
    EasyGeofencing.startGeofenceService(
        pointedLatitude: latitude.toString(),
        pointedLongitude: longitude.toString(),
        radiusMeter: "4.0",
        eventPeriodInSeconds: 5);
  }
}
