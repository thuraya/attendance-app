import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class AuthService {
  FlutterSecureStorage flutterSecureStorage;
  AuthService() {
    flutterSecureStorage = FlutterSecureStorage();
  }

  Future<bool> checkAuthentication() async {
    String accessToken = await flutterSecureStorage.read(key: 'access_token');
    if (accessToken != null) return true;
    return false;
  }
}
