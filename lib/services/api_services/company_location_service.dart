import 'dart:convert';

import 'package:attendance_logging_app/env.dart';
import 'package:http/http.dart' as http;

class CompanyLocationService {
  Future<http.Response> getCompanyLocation() async {
    return http.get(Uri.parse(Api.baseUrl + Api.getCompanyLocation));
  }

  Future<http.Response> setCompanyLocation(
      String latitude, String longitude) async {
    return http.post(
      Uri.parse(Api.baseUrl + Api.setCompanyLocation),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(
          <String, String>{'LocationX': latitude, 'LocationY': longitude}),
    );
  }
}
