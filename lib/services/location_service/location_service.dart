import 'dart:async';

import 'package:attendance_logging_app/models/user_location.dart';
import 'package:geolocator/geolocator.dart';
//import 'package:location/location.dart';

class LocationService {
  UserLocation _currentLocation;

  StreamController<UserLocation> _locationController =
      StreamController<UserLocation>();

  Stream<UserLocation> get locationStream => _locationController.stream;

  LocationService() {
    // Request permission to use location

    Geolocator.requestPermission().then(
      (status) {
        if (status == LocationPermission.always) {
          // If granted listen to the onLocationChanged stream and emit over our controller
          Geolocator.getPositionStream(
            desiredAccuracy: LocationAccuracy.best,
            intervalDuration: Duration(
              seconds: 1,
            ),
          ).listen((locationData) {
            if (locationData != null) {
              // easyGeofencing = EasyGeofencing.startGeofenceService(
              //   pointedLatitude: locationData.latitude.toString(),
              //   pointedLongitude: locationData.longitude.toString(),
              //   radiusMeter: "4.0",
              //   eventPeriodInSeconds: 2,
              // );
              _locationController.add(UserLocation(
                latitude: locationData.latitude,
                longitude: locationData.longitude,
              ));

              // EasyGeofencing.getGeofenceStream().listen((event) {
              //   if (event == GeofenceStatus.exit) {}
              // });
            }
          });
        }
      },
    );
  }
}
