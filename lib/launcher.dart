import 'package:flutter/material.dart';

import 'presentation/presentation.dart';
import 'services/services.dart';

class Launcher extends StatefulWidget {
  const Launcher({Key key}) : super(key: key);

  @override
  _LauncherState createState() => _LauncherState();
}

class _LauncherState extends State<Launcher> {
  bool isAuth = false;
  @override
  void initState() {
    super.initState();
    _checkAuth();
  }

  void _checkAuth() async {
    AuthService authService = AuthService();
    bool auth = await authService.checkAuthentication();
    if (auth) {
      setState(() {
        isAuth = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    Widget child;
    //TODO: REMOVE THE !
    if (isAuth) {
      child = Home();
    } else {
      child = Login();
    }

    return child;
  }
}

//Scenario
// 1***Login Screen***: [Login Button] --> 2***HistoryCheck*** [Create Account] -->
// 3***Sign Up***
//Sign Up Screen: [Next] --> 4***Face ID*** --> 5***Shifts*** --> 6***Done*** --> (Checks) on first launch

//HistoryCheck [Go to salary] --> 7***Salary*** [Go to check] --> 8***Checks***
//Salary [Go to check] --> Checks
//Checks [Check In]/[Check Out] --> 9***Locate Employee*** --> Face ID -->
// 10***Check In/Out Successfully***
