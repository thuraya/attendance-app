class Api {
  static const String https = 'https://';
  static const String domain = '.gymesis.net/';

  static const String baseUrl = Api.https + "www.gymesis.net/";

  static const String login = '/login';
  static const String changePassword = '/changePassword';
  static const String getCompanyLocation = 'api/department';
  static const String setCompanyLocation = 'api/department/1';
}
