import 'package:attendance_logging_app/models/user_location.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import 'launcher.dart';
import 'presentation/presentation.dart';
import 'presentation/styles/styles.dart';
import 'services/services.dart';

void setupLocator() {
  locator.registerLazySingleton(() => NavigationService());
}

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  setupLocator();
  runApp(App());
}

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamProvider<UserLocation>(
      create: (context) => LocationService().locationStream,
      child: MaterialApp(
        title: 'Attendance App',
        navigatorKey: locator<NavigationService>().navigatorKey,
        debugShowCheckedModeBanner: false,
        color: DesignColors.DARK_COLOR1,
        theme: themeData,
        routes: {
          '/launcher': (context) => Launcher(),
          '/home': (context) => Home(),
          '/company_location': (context) => CompanyLocation(),
          '/history_check': (context) => HistoryCheck(),
          '/salary': (context) => Salary(),
          '/location': (context) => Location(),
          '/face_id': (context) => FaceID(),
          '/login': (context) => Login(),
          '/register': (context) => Register(),
        },
        initialRoute: '/launcher',
      ),
    );
  }
}
