class SalaryModel {
  String month;
  String amount;
  String state;
  SalaryModel({this.month, this.amount, this.state});
}

List<SalaryModel> salaries = [
  SalaryModel(month: "Jan", amount: "1400000", state: "More"),
  SalaryModel(month: "Feb", amount: "1200000", state: "More"),
  SalaryModel(month: "Mar", amount: "1000000", state: "Same"),
  SalaryModel(month: "Apr", amount: "1000000", state: "Same"),
  SalaryModel(month: "May", amount: "1000000", state: "Same"),
  SalaryModel(month: "Jun", amount: "950000", state: "Less"),
  SalaryModel(month: "Jul", amount: "950000", state: "Less"),
  SalaryModel(month: "Aug", amount: "900000", state: "Less"),
  SalaryModel(month: "Sep", amount: "1000000", state: "Same"),
  SalaryModel(month: "Oct", amount: "1000000", state: "Same"),
  SalaryModel(month: "Nov", amount: "1000000", state: "Same"),
  SalaryModel(month: "Dec", amount: "1300000", state: "More"),
];
