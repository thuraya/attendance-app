class CheckIn {
  String date;
  String entranceTime;
  String exitTime;
  CheckIn({this.date, this.entranceTime, this.exitTime});
}

List<CheckIn> checkIns = [
  CheckIn(
      date: 'Thu 01-09-2021', entranceTime: '08:30 A.M', exitTime: '04:31 P.M'),
  CheckIn(
      date: 'Fri 02-09-2021', entranceTime: '09:30 A.M', exitTime: '05:15 P.M'),
  CheckIn(
      date: 'Sat 03-09-2021', entranceTime: '09:02 A.M', exitTime: '05:07 P.M'),
  CheckIn(
      date: 'Sun 04-09-2021', entranceTime: '09:04 A.M', exitTime: '04:59 P.M'),
  CheckIn(
      date: 'Mon 05-09-2021', entranceTime: '09:00 A.M', exitTime: '04:45 P.M'),
  CheckIn(
      date: 'Tue 06-09-2021', entranceTime: '09:05 A.M', exitTime: '05:11 P.M'),
  CheckIn(
      date: 'Wed 07-09-2021', entranceTime: '09:00 A.M', exitTime: '05:13 P.M'),
  CheckIn(
      date: 'Thu 08-09-2021', entranceTime: '08:58 A.M', exitTime: '05:09 P.M'),
  CheckIn(
      date: 'Fri 09-09-2021', entranceTime: '08:59 A.M', exitTime: '05:03 P.M'),
  CheckIn(
      date: 'Sat 10-09-2021', entranceTime: '08:44 A.M', exitTime: '05:00 P.M'),
  CheckIn(
      date: 'Sun 11-09-2021', entranceTime: '08:47 A.M', exitTime: '05:06 P.M'),
  CheckIn(
      date: 'Mon 12-09-2021', entranceTime: '08:34 A.M', exitTime: '05:11 P.M'),
  CheckIn(
      date: 'Tue 13-09-2021', entranceTime: '10:55 A.M', exitTime: '05:51 P.M'),
  CheckIn(
      date: 'Tue 14-09-2021', entranceTime: '08:30 A.M', exitTime: '05:01 P.M'),
];
