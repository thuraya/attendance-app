import 'package:attendance_logging_app/models/user_location.dart';
import 'package:attendance_logging_app/services/navigation_service/navigation_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../styles/styles.dart';
import '../../widgets/widgets.dart';

class Location extends StatelessWidget {
  const Location({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double fontSize = height * FONT16;
    var userLocation = Provider.of<UserLocation>(context);
    return SafeArea(
      child: Scaffold(
        backgroundColor: DesignColors.BACKGROUND,
        body: LayoutBuilder(
          builder: (context, constraints) {
            return Container(
              child: Center(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: constraints.maxHeight * 0.1,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Padding(
                            padding: EdgeInsets.only(
                              right: constraints.maxWidth * 0.04,
                            ),
                            child: IconButton(
                              onPressed: () {
                                showDialog(
                                  context: context,
                                  builder: (context) {
                                    return Dialog(
                                      child: SizedBox(
                                        height: constraints.maxHeight * 0.3,
                                        width: constraints.maxWidth * 0.5,
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceAround,
                                          children: [
                                            Padding(
                                              padding: EdgeInsets.symmetric(
                                                  horizontal:
                                                      constraints.maxWidth *
                                                          0.06,
                                                  vertical:
                                                      constraints.maxHeight *
                                                          0.06),
                                              child: Text(
                                                'Do you want to set this location as your working place location?',
                                                style: TextStyle(
                                                    color: DesignColors
                                                        .LIGHT_GREY2,
                                                    fontWeight: FontWeight.w500,
                                                    fontSize: fontSize),
                                              ),
                                            ),
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.end,
                                              children: [
                                                TextButton(
                                                  onPressed: () {
                                                    //TODO: Setting this location for as a work place for geo fencing
                                                  },
                                                  child: Text(
                                                    'YES',
                                                    style: TextStyle(
                                                      color: DesignColors.BLACK,
                                                      fontWeight:
                                                          FontWeight.w600,
                                                    ),
                                                  ),
                                                ),
                                                TextButton(
                                                  onPressed: () {
                                                    locator<NavigationService>()
                                                        .navigatePop();
                                                  },
                                                  child: Text(
                                                    'NO',
                                                    style: TextStyle(
                                                      color: DesignColors.RED,
                                                      fontWeight:
                                                          FontWeight.w600,
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ),
                                    );
                                  },
                                );
                              },
                              icon: Icon(
                                CupertinoIcons.pin_fill,
                                color: DesignColors.BLUE,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      height: constraints.maxHeight * 0.38,
                      width: constraints.maxWidth * 0.88,
                      decoration: BoxDecoration(
                        borderRadius: locationGifBorderRadius,
                        color: DesignColors.WHITE,
                        image: DecorationImage(
                          image: AssetImage('assets/gifs/location1.gif'),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: constraints.maxHeight * 0.04,
                    ),
                    Container(
                      width: constraints.maxWidth * 0.8,
                      child: Text(
                        'We need to determine your location',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: DesignColors.WHITE,
                          fontSize: fontSize,
                        ),
                      ),
                    ),
                    Container(
                      width: constraints.maxWidth * 0.8,
                      child: Text(
                        'Click OK to help us locate you',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: DesignColors.WHITE,
                          fontSize: fontSize,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: constraints.maxHeight * 0.05,
                    ),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            '[  Latitude: ',
                            style: TextStyle(
                              color: DesignColors.WHITE,
                              fontSize: fontSize,
                            ),
                          ),
                          Text(
                            userLocation == null
                                ? '0'
                                : userLocation.latitude.toString(),
                            style: TextStyle(
                              color: DesignColors.WHITE,
                              fontSize: fontSize,
                            ),
                          ),
                          Text(
                            '\t\t\tLongitude: ',
                            style: TextStyle(
                              color: DesignColors.WHITE,
                              fontSize: fontSize,
                            ),
                          ),
                          Text(
                            userLocation == null
                                ? '0'
                                : userLocation.longitude.toString(),
                            style: TextStyle(
                              color: DesignColors.WHITE,
                              fontSize: fontSize,
                            ),
                          ),
                          Text(
                            '  ]',
                            style: TextStyle(
                              color: DesignColors.WHITE,
                              fontSize: fontSize,
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: constraints.maxHeight * 0.05,
                    ),
                    AcceptBtn(
                      function: () {
                        locator<NavigationService>()
                            .navigateToNamed('/face_id');
                      },
                    ),
                    SizedBox(
                      height: constraints.maxHeight * 0.025,
                    ),
                    DenyLocationAccessBtn(
                        // function: () async {
                        //   //GeolocatorPlatform.instance.requestPermission();
                        // },
                        ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
