import 'package:flutter/material.dart';

import '../../../services/services.dart';
import '../../widgets/widgets.dart';

class RegisterForm extends StatefulWidget {
  const RegisterForm({Key key}) : super(key: key);

  @override
  _RegisterFormState createState() => _RegisterFormState();
}

class _RegisterFormState extends State<RegisterForm> {
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController nameController = TextEditingController();
  TextEditingController dateOfBirthController = TextEditingController();
  TextEditingController occupationController = TextEditingController();
  TextEditingController maritalStatusController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  bool loading = false;
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return Container(
      height: height,
      width: width * 0.944,
      child: Form(
        key: _formKey,
        child: Column(
          children: [
            CustomizedTextField(
              icon: Icons.person_outline,
              hintText: "\t\tName",
              textInputAction: TextInputAction.next,
              validator: (email) {
                if (email.isEmpty)
                  return 'Name is required';
                else
                  return null;
              },
              controller: nameController,
              formState: _formKey.currentState,
            ),
            CustomizedTextField(
              icon: Icons.cake,
              hintText: "\t\tDate of birth",
              textInputAction: TextInputAction.next,
              validator: (email) {
                if (email.isEmpty)
                  return 'Date of birth is required';
                else
                  return null;
              },
              controller: dateOfBirthController,
              formState: _formKey.currentState,
            ),
            CustomizedTextField(
              icon: Icons.expand_more,
              hintText: "\t\tOccupation",
              textInputAction: TextInputAction.next,
              validator: (email) {
                if (email.isEmpty)
                  return 'Occupation is required';
                else
                  return null;
              },
              controller: occupationController,
              formState: _formKey.currentState,
            ),
            CustomizedTextField(
              icon: Icons.expand_more,
              hintText: "\t\tMarital Status",
              textInputAction: TextInputAction.next,
              validator: (email) {
                if (email.isEmpty)
                  return 'Marital Status is required';
                else
                  return null;
              },
              controller: maritalStatusController,
              formState: _formKey.currentState,
            ),
            CustomizedTextField(
              icon: Icons.alternate_email,
              hintText: "\t\tEmail",
              textInputAction: TextInputAction.next,
              validator: (email) {
                if (email.isEmpty)
                  return 'Email is required';
                else
                  return null;
              },
              controller: emailController,
              formState: _formKey.currentState,
            ),
            PasswordTextField(
              hintText: "\t\tPassword",
              textInputAction: TextInputAction.done,
              validator: (password) {
                RegExp notShort = RegExp(r'.{6,}');

                if (password.isEmpty) return 'Password is required';
                if (!notShort.hasMatch(password))
                  return 'Password must have at least 6 characters';
                return null;
              },
              controller: passwordController,
              formState: _formKey.currentState,
            ),
            NextButton(
              onTap: () {
                handleRegistration();
              },
            ),
          ],
        ),
      ),
    );
  }

  void handleRegistration() async {
    String toastMessage;
    final FormState form = _formKey.currentState;
    setState(() {
      loading = true;
    });
    Future<void>.delayed(Duration(seconds: 1)).then((value) {
      if (!form.validate()) {
      } else {
        form.save();
        toastMessage = 'Logged in successfully';
        setState(() {
          loading = false;
        });
        showToast(toastMessage, context);
      }
    });
    locator<NavigationService>().navigateToNamed('/face_id');
  }
}
