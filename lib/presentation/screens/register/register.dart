import 'package:flutter/material.dart';

import '../../styles/styles.dart';
import '../../widgets/widgets.dart';

class Register extends StatelessWidget {
  const Register({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double titleFont = height * FONT18;
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: DesignColors.BACKGROUND,
          centerTitle: false,
          title: Text(
            'Registration Form',
            style: TextStyle(
              color: DesignColors.WHITE,
              fontSize: titleFont,
            ),
          ),
        ),
        backgroundColor: DesignColors.BACKGROUND,
        body: SingleChildScrollView(
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    RegisterHeader(),
                  ],
                ),
                RegisterForm(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
