import 'package:flutter/material.dart';

import '../../styles/styles.dart';

class RegisterHeader extends StatelessWidget {
  const RegisterHeader({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    double headerSize = height * FONT14;

    return Padding(
      padding: EdgeInsets.only(
        bottom: height * 0.04,
        left: width * 0.125,
      ),
      child: Text(
        'Please fill you\'re information.',
        style: TextStyle(
          fontWeight: FontWeight.w600,
          color: DesignColors.LIGHT_GREY2,
          fontSize: headerSize,
        ),
      ),
    );
  }
}

// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:point_of_sale/styles/colors.dart';
//
// import '../../styles/styles.dart';
//
// class LoginDecoratedHeader extends StatelessWidget {
//   const LoginDecoratedHeader({Key key}) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return Column(
//       mainAxisAlignment: MainAxisAlignment.center,
//       crossAxisAlignment: CrossAxisAlignment.center,
//       children: [
//         Stack(
//           children: [
//             Positioned(
//               // bottom: 0,
//               // right: 38,
//               child: Center(
//                 child: Image.asset(
//                   'assets/images/cloud.png',
//                   height: ScreenUtil().setHeight(220),
//                 ),
//               ),
//             ),
//             Positioned(
//               bottom: 68.h,
//               right: 54.w,
//               child: Center(
//                 child: Container(
//                   margin: EdgeInsets.only(top: 0.0, bottom: 8.0),
//                   child: Text(
//                     'Arison Perfumes',
//                     textAlign: TextAlign.center,
//                     style: TextStyle(
//                       color: AppColor.BLACK,
//                       fontWeight: FontWeight.w700,
//                       fontSize: 30.sp,
//                       fontFamily: 'WindSong',
//                     ),
//                   ),
//                 ),
//               ),
//             ),
//           ],
//         ),
//       ],
//     );
//   }
// }
