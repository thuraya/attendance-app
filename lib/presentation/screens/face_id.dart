import 'package:flutter/material.dart';

import '../styles/styles.dart';
import '../widgets/widgets.dart';

class FaceID extends StatelessWidget {
  const FaceID({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Face ID'),
      ),
      body: Container(
        decoration: BoxDecoration(
          color: DesignColors.BACKGROUND,
        ),
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Card(
                elevation: 10,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Image.asset(
                    'assets/images/face.jpg',
                    height: 270,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(
                  vertical: 10.0,
                  horizontal: 10.0,
                ),
                child: Text(
                  'Place your face within the rectangle to help us identify you',
                  style: TextStyle(
                    color: DesignColors.WHITE,
                    fontSize: 10.0,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(
                  vertical: 6.0,
                ),
                child: Column(
                  children: [
                    CameraBtn(),
                    SizedBox(
                      height: 40.0,
                    ),
                    NextButton(
                      onTap: () {
                        showDialog(
                          context: context,
                          builder: (context) {
                            return CheckingDoneDialog();
                          },
                        );
                      },
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
