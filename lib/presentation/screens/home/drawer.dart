import 'package:attendance_logging_app/presentation/styles/colors.dart';
import 'package:attendance_logging_app/presentation/styles/font_constants.dart';
import 'package:attendance_logging_app/services/navigation_service/navigation_service.dart';
import 'package:flutter/material.dart';

class CustomizedDrawer extends StatelessWidget {
  const CustomizedDrawer({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double topPadding = MediaQuery.of(context).padding.top;
    double bottomPadding = MediaQuery.of(context).padding.bottom;
    return SafeArea(
      child: LayoutBuilder(
        builder: (context, constraints) {
          double height = constraints.maxHeight - topPadding - bottomPadding;
          double width = constraints.maxWidth;
          double fontSize = height * FONT19;
          return Container(
            height: height,
            width: width * 0.88,
            decoration: BoxDecoration(
              color: DesignColors.DARK_COLOR2,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Column(
                  children: [
                    SizedBox(
                      height: height * 0.3,
                      child: Container(
                        decoration: BoxDecoration(
                          color: DesignColors.BLACK.withOpacity(0.4),
                        ),
                      ),
                    ),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    _buildDrawerOptions(
                      color: DesignColors.BLACK.withOpacity(0.4),
                      width: width,
                      height: height,
                      text: 'Set Company Location',
                      fontSize: fontSize,
                      function: () {
                        locator<NavigationService>()
                            .navigateToNamed('/company_location');
                      },
                    ),
                    _buildDrawerOptions(
                      color: DesignColors.DARK_COLOR1,
                      width: width,
                      height: height,
                      text: 'Check In History',
                      fontSize: fontSize,
                      function: () {
                        locator<NavigationService>()
                            .navigateToNamed('/history_check');
                      },
                    ),
                    _buildDrawerOptions(
                      color: DesignColors.BLACK.withOpacity(0.4),
                      width: width,
                      height: height,
                      text: 'Salary History',
                      fontSize: fontSize,
                      function: () {
                        locator<NavigationService>().navigateToNamed('/salary');
                      },
                    ),
                    _buildDrawerOptions(
                      color: DesignColors.DARK_COLOR1,
                      width: width,
                      height: height,
                      text: 'Notifications',
                      fontSize: fontSize,
                      function: () {
                        print('Notifications');
                      },
                    ),
                    _buildDrawerOptions(
                      color: DesignColors.BLACK.withOpacity(0.4),
                      width: width,
                      height: height,
                      text: 'Logout',
                      fontSize: fontSize,
                      function: () {
                        locator<NavigationService>()
                            .navigateAndClearStack('/login');
                      },
                    ),
                  ],
                ),
              ],
            ),
          );
        },
      ),
    );
  }

  _buildDrawerOptions(
      {String text,
      double fontSize,
      Function function,
      double width,
      double height,
      Color color}) {
    return InkWell(
      onTap: () {
        function();
      },
      child: Container(
        alignment: Alignment.center,
        width: width * 0.88,
        padding: EdgeInsets.symmetric(
          // horizontal: 20.0,
          vertical: height * 0.028,
        ),
        decoration: BoxDecoration(
          color: color,
        ),
        child: Text(
          text,
          style: TextStyle(
            color: DesignColors.BLUE,
            fontSize: fontSize,
            letterSpacing: 1.0,
          ),
        ),
      ),
    );
  }

  _buildLogoutButton(
      {String text, double fontSize, Function function, double width}) {
    return InkWell(
      onTap: () {
        function();
      },
      child: Container(
        alignment: Alignment.center,
        width: width * 0.88 * 0.4,
        margin: EdgeInsets.symmetric(
          vertical: 8.0,
        ),
        padding: EdgeInsets.symmetric(
          horizontal: 20.0,
          vertical: 10.0,
        ),
        decoration: BoxDecoration(
          color: DesignColors.BLACK.withOpacity(0.4),
          borderRadius: BorderRadius.circular(16.0),
        ),
        child: Text(
          text,
          style: TextStyle(color: DesignColors.WHITE, fontSize: fontSize),
        ),
      ),
    );
  }
}
