import 'package:attendance_logging_app/presentation/screens/home/drawer.dart';
import 'package:attendance_logging_app/presentation/styles/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../services/services.dart';

class Home extends StatefulWidget {
  const Home({Key key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final GlobalKey<ScaffoldState> _key = GlobalKey();

  @override
  Widget build(BuildContext context) {
    double topPadding = MediaQuery.of(context).padding.top;
    double bottomPadding = MediaQuery.of(context).padding.bottom;
    return SafeArea(child: LayoutBuilder(
      builder: (context, constraints) {
        double height = constraints.maxHeight - topPadding - bottomPadding;
        double width = constraints.maxWidth;
        return Scaffold(
          key: _key,
          appBar: AppBar(
            automaticallyImplyLeading: false,
            leadingWidth: width * 0.5,
            backgroundColor: DesignColors.WHITE,
            leading: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                IconButton(
                  icon: Icon(
                    CupertinoIcons.rectangle_3_offgrid_fill,
                    color: DesignColors.BACKGROUND,
                    size: height * 0.052,
                  ),
                  onPressed: () {
                    _key.currentState.openDrawer();
                  },
                ),
                IconButton(
                  onPressed: () {
                    locator<NavigationService>()
                        .navigateToNamed('/history_check');
                  },
                  icon: Icon(
                    Icons.history,
                    color: DesignColors.BACKGROUND,
                    size: height * 0.052,
                  ),
                )
              ],
            ),
          ),
          drawer: CustomizedDrawer(),
          backgroundColor: DesignColors.BACKGROUND,
          body: Container(
            color: DesignColors.WHITE,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text('Press the button below to CHECK IN'),
                InkWell(
                  onTap: () {
                    locator<NavigationService>().navigateToNamed('/location');
                  },
                  child: Container(
                    margin: EdgeInsets.symmetric(horizontal: width * 0.28),
                    decoration: BoxDecoration(
                      color: DesignColors.WHITE,
                      borderRadius: BorderRadius.circular(32.0),
                      image: DecorationImage(
                        image: AssetImage('assets/gifs/check-in.gif'),
                      ),
                    ),
                    child: SizedBox(
                      height: height * 0.3,
                      width: double.infinity,
                    ),
                  ),
                ),
                Text('Press the button below to CHECK OUT'),
                InkWell(
                  onTap: () {
                    locator<NavigationService>().navigateToNamed('/location');
                  },
                  child: Container(
                    margin: EdgeInsets.symmetric(horizontal: width * 0.28),
                    decoration: BoxDecoration(
                        color: DesignColors.WHITE,
                        borderRadius: BorderRadius.circular(32.0),
                        image: DecorationImage(
                            image: AssetImage('assets/gifs/check-out.gif'))),
                    child: SizedBox(
                      height: height * 0.3,
                      width: double.infinity,
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    ));
  }
}
