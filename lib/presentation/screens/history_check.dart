import 'package:attendance_logging_app/services/navigation_service/navigation_service.dart';
import 'package:attendance_logging_app/testing_samples/check_in_sample_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../styles/styles.dart';

class HistoryCheck extends StatelessWidget {
  const HistoryCheck({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: DesignColors.WHITE,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: DesignColors.BACKGROUND,
          ),
          onPressed: () {
            locator<NavigationService>().navigatePop();
          },
        ),
        title: Text(
          'Check Ins History',
          style: TextStyle(
            color: DesignColors.BACKGROUND,
          ),
        ),
      ),
      body: Container(
        child: Column(
          children: [
            Container(
              child: Padding(
                padding: const EdgeInsets.symmetric(
                    horizontal: 12.0, vertical: 10.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [Icon(CupertinoIcons.calendar_today)],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(CupertinoIcons.clock),
                      ],
                    )
                  ],
                ),
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(12.0, 0, 12.0, 0),
                child: Card(
                  elevation: 2,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16.0),
                    child: ListView.builder(
                      itemCount: checkIns.length,
                      itemBuilder: (context, index) {
                        return CheckInRow(
                          checkIn: checkIns[index],
                          color: index % 2 == 0
                              ? DesignColors.LIGHT_GREY2.withOpacity(0.2)
                              : DesignColors.WHITE,
                        );
                      },
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: Column(
                children: [
                  // GoToSalaryBtn(
                  //   function: () {
                  //     locator<NavigationService>()
                  //         .navigateAndClearStack('/salary');
                  //   },
                  // ),
                  // GoToCheckBtn(
                  //   function: () {
                  //     locator<NavigationService>()
                  //         .navigateAndClearStack('/home');
                  //   },
                  // ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

class CheckInRow extends StatelessWidget {
  final Color color;
  final CheckIn checkIn;
  const CheckInRow({Key key, this.checkIn, this.color}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
      color: color,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    checkIn?.date ?? " - ",
                  ),
                ],
              ),
            ],
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  // Text('In: '),
                  Text(
                    checkIn?.entranceTime ?? " - ",
                  ),
                  // Text(' Out: '),
                  Text('  '),
                  Text(
                    checkIn?.exitTime ?? " - ",
                  ),
                ],
              ),
            ],
          )
        ],
      ),
    );
  }
}
