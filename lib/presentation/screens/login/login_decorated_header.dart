import 'package:flutter/material.dart';

import '../../styles/styles.dart';

class LoginDecoratedHeader extends StatelessWidget {
  const LoginDecoratedHeader({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    double headerSize = height * FONT30;
    return Padding(
      padding: EdgeInsets.only(
        left: width * 0.125,
        right: width * 0.125,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.only(
              bottom: height * 0.02,
            ),
            child: Text(
              'Let\'s sign you in.',
              style: TextStyle(
                letterSpacing: 2.72,
                fontWeight: FontWeight.w700,
                color: DesignColors.WHITE,
                fontSize: headerSize,
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text(
                'Welcome back.',
                style: TextStyle(
                  letterSpacing: 2.72,
                  fontWeight: FontWeight.w300,
                  color: DesignColors.WHITE,
                  fontSize: headerSize,
                ),
              ),
            ],
          ),
          Padding(
            padding: EdgeInsets.only(
              bottom: height * 0.02 * 2.5,
            ),
            child: Text(
              'You\'ve been missed!',
              style: TextStyle(
                letterSpacing: 2.72,
                fontWeight: FontWeight.w300,
                color: DesignColors.WHITE,
                fontSize: headerSize,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:point_of_sale/styles/colors.dart';
//
// import '../../styles/styles.dart';
//
// class LoginDecoratedHeader extends StatelessWidget {
//   const LoginDecoratedHeader({Key key}) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return Column(
//       mainAxisAlignment: MainAxisAlignment.center,
//       crossAxisAlignment: CrossAxisAlignment.center,
//       children: [
//         Stack(
//           children: [
//             Positioned(
//               // bottom: 0,
//               // right: 38,
//               child: Center(
//                 child: Image.asset(
//                   'assets/images/cloud.png',
//                   height: ScreenUtil().setHeight(220),
//                 ),
//               ),
//             ),
//             Positioned(
//               bottom: 68.h,
//               right: 54.w,
//               child: Center(
//                 child: Container(
//                   margin: EdgeInsets.only(top: 0.0, bottom: 8.0),
//                   child: Text(
//                     'Arison Perfumes',
//                     textAlign: TextAlign.center,
//                     style: TextStyle(
//                       color: AppColor.BLACK,
//                       fontWeight: FontWeight.w700,
//                       fontSize: 30.sp,
//                       fontFamily: 'WindSong',
//                     ),
//                   ),
//                 ),
//               ),
//             ),
//           ],
//         ),
//       ],
//     );
//   }
// }
