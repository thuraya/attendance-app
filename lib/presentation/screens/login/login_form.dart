import 'package:flutter/material.dart';

import '../../../services/services.dart';
import '../../styles/styles.dart';
import '../../widgets/widgets.dart';

class LoginForm extends StatefulWidget {
  const LoginForm({Key key}) : super(key: key);

  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  bool loading = false;
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double fontSize = height * FONT14;
    double width = MediaQuery.of(context).size.width;

    return Container(
      height: height * 0.6,
      width: width * 0.944,
      child: Form(
        key: _formKey,
        child: Column(
          children: [
            CustomizedTextField(
              icon: Icons.alternate_email,
              hintText: "\t\tEmail",
              textInputAction: TextInputAction.next,
              validator: (email) {
                if (email.isEmpty)
                  return 'Email is required';
                else
                  return null;
              },
              controller: emailController,
              formState: _formKey.currentState,
            ),
            PasswordTextField(
              hintText: "\t\tPassword",
              textInputAction: TextInputAction.done,
              validator: (password) {
                RegExp notShort = RegExp(r'.{6,}');
                if (password.isEmpty) return 'Password is required';
                if (!notShort.hasMatch(password))
                  return 'Password must have at least 6 characters';
                return null;
              },
              controller: passwordController,
              formState: _formKey.currentState,
            ),
            Flexible(
              fit: FlexFit.loose,
              child: SizedBox(
                height: height * 0.24,
              ),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: height * 0.025),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Don\'t have an account? ',
                    style: TextStyle(
                      color: DesignColors.WHITE,
                      fontSize: fontSize,
                      fontWeight: FontWeight.w300,
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      locator<NavigationService>().navigateToNamed('/register');
                    },
                    child: Text(
                      'Register',
                      style: TextStyle(
                        color: DesignColors.WHITE,
                        fontSize: fontSize,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            LoginButton(
              onTap: () {
                handleLogin(emailController.text, passwordController.text);
              },
            ),
          ],
        ),
      ),
    );
  }

  void handleLogin(String username, String password) async {
    String toastMessage;
    final FormState form = _formKey.currentState;
    setState(() {
      loading = true;
    });
    Future<void>.delayed(Duration(seconds: 1)).then((value) {
      if (!form.validate()) {
      } else {
        form.save();
        toastMessage = 'Logged in successfully';
        setState(() {
          loading = false;
        });
        showToast(toastMessage, context);
      }
    });
    locator<NavigationService>().navigateAndClearStack('/home');
  }
}
