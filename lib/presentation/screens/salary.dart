import 'package:attendance_logging_app/testing_samples/salary_sample_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../services/services.dart';
import '../styles/styles.dart';

class Salary extends StatelessWidget {
  const Salary({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: DesignColors.WHITE,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: DesignColors.BACKGROUND,
          ),
          onPressed: () {
            locator<NavigationService>().navigatePop();
          },
        ),
        title: Text(
          'Salary History',
          style: TextStyle(
            color: DesignColors.BACKGROUND,
          ),
        ),
      ),
      body: LayoutBuilder(
        builder: (context, constraints) {
          return Container(
            color: DesignColors.WHITE,
            child: Column(
              children: [
                // Container(
                //   child: Padding(
                //     padding: const EdgeInsets.symmetric(
                //         horizontal: 20.0, vertical: 10.0),
                //     child: Row(
                //       mainAxisAlignment: MainAxisAlignment.start,
                //       children: [
                //         Text(
                //           '2020',
                //           style: TextStyle(
                //             color: DesignColors.BLACK,
                //             fontSize: 20.0,
                //             fontWeight: FontWeight.w600,
                //           ),
                //         ),
                //       ],
                //     ),
                //   ),
                // ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(12.0, 0, 12.0, 0),
                    child: Card(
                      elevation: 2,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 16.0),
                        child: ListView.builder(
                          itemCount: salaries.length,
                          itemBuilder: (context, index) {
                            return SalaryRow(
                              salary: salaries[index],
                              color: index % 2 == 0
                                  ? DesignColors.LIGHT_GREY2.withOpacity(0.2)
                                  : DesignColors.WHITE,
                            );
                          },
                        ),
                      ),
                    ),
                  ),
                ),
                // Padding(
                //   padding: const EdgeInsets.only(bottom: 8.0),
                //   child: Column(
                //     children: [
                //       GoToCheckBtn(
                //         function: () {
                //           locator<NavigationService>()
                //               .navigateToNamed('/home');
                //         },
                //       ),
                //     ],
                //   ),
                // )
              ],
            ),
          );
        },
      ),
    );
  }
}

class SalaryRow extends StatelessWidget {
  final Color color;
  final SalaryModel salary;
  const SalaryRow({Key key, this.salary, this.color}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    double fontSizeLeading = height * FONT14;
    double fontSizeLeading1 = height * 0.1;
    // return Card(
    //   color: DesignColors.TEXT_FIELD_FILL,
    //   child: ListTile(
    //     leading: Container(
    //       margin: EdgeInsets.symmetric(
    //         vertical: height * 0.01,
    //         horizontal: width * 0.02,
    //       ),
    //       padding: EdgeInsets.symmetric(
    //         vertical: height * 0.02,
    //         horizontal: width * 0.02,
    //       ),
    //       height: fontSizeLeading1,
    //       decoration: BoxDecoration(
    //         color: DesignColors.BACKGROUND,
    //         shape: BoxShape.circle,
    //       ),
    //       child: Column(
    //         mainAxisAlignment: MainAxisAlignment.center,
    //         children: [
    //           Text(
    //             salary?.month ?? " - ",
    //             // textAlign: TextAlign.center,
    //             style: TextStyle(
    //               color: DesignColors.WHITE,
    //               fontWeight: FontWeight.w700,
    //               fontSize: fontSizeLeading,
    //               letterSpacing: 1.0,
    //             ),
    //           ),
    //         ],
    //       ),
    //     ),
    //     trailing: Text(
    //       " - ",
    //     ),
    //   ),
    // );
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
      color: color,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    salary?.month ?? " - ",
                  ),
                ],
              ),
            ],
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  // Text('In: '),
                  Text(
                    salary?.amount ?? " - ",
                  ),
                  // Text(' Out: '),
                  Text('  '),
                  if (salary.state.contains('Same'))
                    SizedBox(
                      child: Icon(
                        Icons.expand_less,
                        color: Colors.transparent,
                      ),
                    )
                  else if (salary.state.contains('More'))
                    SizedBox(
                      child: Icon(
                        Icons.expand_less,
                        color: DesignColors.GREEN,
                      ),
                    )
                  else
                    SizedBox(
                      child: Icon(Icons.expand_more, color: DesignColors.RED),
                    )
                ],
              ),
            ],
          )
        ],
      ),
    );
  }
}
