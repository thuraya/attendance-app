import 'dart:async';

import 'package:attendance_logging_app/presentation/presentation.dart';
import 'package:attendance_logging_app/presentation/widgets/customized_loading_indicator.dart';
import 'package:attendance_logging_app/services/navigation_service/navigation_service.dart';
import 'package:easy_geofencing/easy_geofencing.dart';
import 'package:easy_geofencing/enums/geofence_status.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';

class CompanyLocation extends StatefulWidget {
  CompanyLocation({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _CompanyLocationState createState() => _CompanyLocationState();
}

class _CompanyLocationState extends State<CompanyLocation> {
  bool loading = false;
  TextEditingController latitudeController = new TextEditingController();
  TextEditingController longitudeController = new TextEditingController();
  TextEditingController radiusController = new TextEditingController();
  StreamSubscription<GeofenceStatus> geofenceStatusStream;
  Geolocator geolocator = Geolocator();
  String geofenceStatus = '';
  bool isReady = false;
  Position position;
  @override
  void initState() {
    super.initState();
    getCurrentPosition();
  }

  getCurrentPosition() async {
    position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
    print("LOCATION => ${position.toJson()}");
    isReady = (position != null) ? true : false;
  }

  setLocation() async {
    setState(() {
      loading = true;
    });
    await getCurrentPosition();
    setState(() {
      loading = false;
    });
    setState(() {
      latitudeController =
          new TextEditingController(text: position.latitude.toString());
      longitudeController =
          new TextEditingController(text: position.longitude.toString());
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: DesignColors.WHITE,
        title: Text(
          'Set Company Location',
          style: TextStyle(
            fontSize: 16.0,
            color: DesignColors.BACKGROUND,
          ),
        ),
        actions: [
          IconButton(
            icon: Icon(
              Icons.my_location,
              color: DesignColors.BACKGROUND,
            ),
            onPressed: () async {
              if (isReady) {
                setState(() {
                  setLocation();
                });
              }
            },
          ),
        ],
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: DesignColors.BACKGROUND,
          ),
          onPressed: () {
            locator<NavigationService>().navigatePop();
          },
        ),
      ),
      body: loading
          ? Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [CustomizedLoadingIndicator()],
                )
              ],
            )
          : SingleChildScrollView(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                children: <Widget>[
                  TextField(
                    readOnly: true,
                    controller: latitudeController,
                    decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText:
                            'Press the locator at the top-right to set the latitude',
                        hintStyle: TextStyle(
                            color: DesignColors.LIGHT_GREY2, fontSize: 11.0),
                        labelText: 'Latitude',
                        labelStyle: TextStyle(
                          color: DesignColors.BLACK,
                        ),
                        floatingLabelBehavior: FloatingLabelBehavior.always),
                  ),
                  TextField(
                    readOnly: true,
                    controller: longitudeController,
                    decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText:
                            'Press the locator at the top-right to set the longitude',
                        hintStyle: TextStyle(
                            color: DesignColors.LIGHT_GREY2, fontSize: 11.0),
                        labelText: 'Longitude',
                        labelStyle: TextStyle(
                          color: DesignColors.BLACK,
                        ),
                        floatingLabelBehavior: FloatingLabelBehavior.always),
                  ),
                  TextField(
                    keyboardType: TextInputType.numberWithOptions(
                        signed: true, decimal: true),
                    controller: radiusController,
                    decoration: InputDecoration(
                        filled: true,
                        fillColor: DesignColors.LIGHT_GREY2.withOpacity(0.2),
                        border: InputBorder.none,
                        hintText: 'Enter radius in meter',
                        hintStyle: TextStyle(
                          color: DesignColors.LIGHT_GREY2,
                        ),
                        labelText: 'Radius (in meters)',
                        labelStyle: TextStyle(
                          color: DesignColors.BLACK,
                        ),
                        floatingLabelBehavior: FloatingLabelBehavior.always),
                  ),
                  SizedBox(
                    height: 60,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      TextButton(
                        style: TextButton.styleFrom(
                          primary: DesignColors.WHITE,
                          backgroundColor: DesignColors.DARK_COLOR1,
                          textStyle: TextStyle(
                            fontSize: 16.0,
                          ),
                          padding: EdgeInsets.symmetric(
                            horizontal: 16.0,
                          ),
                          elevation: 5,
                        ),
                        child: Text("Start"),
                        onPressed: () {
                          print("starting geoFencing Service");
                          EasyGeofencing.startGeofenceService(
                              pointedLatitude: latitudeController.text,
                              pointedLongitude: longitudeController.text,
                              radiusMeter: radiusController.text,
                              eventPeriodInSeconds: 1);
                          if (geofenceStatusStream == null) {
                            geofenceStatusStream =
                                EasyGeofencing.getGeofenceStream()
                                    .listen((GeofenceStatus status) {
                              print(status.toString());
                              setState(() {
                                geofenceStatus = status.toString();
                              });
                            });
                          }
                        },
                      ),
                      TextButton(
                        style: TextButton.styleFrom(
                          primary: DesignColors.WHITE,
                          backgroundColor: DesignColors.DARK_COLOR1,
                          textStyle: TextStyle(
                            fontSize: 16.0,
                          ),
                          padding: EdgeInsets.symmetric(
                            horizontal: 16.0,
                          ),
                          elevation: 5,
                        ),
                        child: Text("Stop"),
                        onPressed: () async {
                          longitudeController.clear();
                          latitudeController.clear();
                          radiusController.clear();
                          geofenceStatus = '';
                          //geolocator = Geolocator();
                          isReady = false;
                          setState(() {
                            loading = true;
                          });
                          EasyGeofencing.stopGeofenceService();

                          await geofenceStatusStream.cancel().then((value) {
                            setState(() {
                              loading = false;
                            });
                          });
                          geofenceStatusStream.cancel();
                        },
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 60,
                  ),
                  Text(
                    // "Geofence Status: \n\n" + geofenceStatus,
                    formatGeoFenceStatus(geofenceStatus),
                    textAlign: TextAlign.center,
                    style:
                        TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ),
    );
  }

  @override
  void dispose() {
    latitudeController.dispose();
    longitudeController.dispose();
    radiusController.dispose();
    geofenceStatusStream.cancel();
    super.dispose();
  }
}

String formatGeoFenceStatus(String geofenceStatus) {
  if (geofenceStatus.contains('init')) return "\t";
  if (geofenceStatus.contains('enter'))
    return "You're inside the company boundaries";
  if (geofenceStatus.contains('exit'))
    return "You're outside the company boundaries";
  return "\t";
}
