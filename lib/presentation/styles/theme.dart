import 'package:flutter/material.dart';

import 'colors.dart';

ThemeData themeData = ThemeData(
  scaffoldBackgroundColor: DesignColors.WHITE,
  appBarTheme: AppBarTheme(
    color: DesignColors.BACKGROUND,
    elevation: 0,
    centerTitle: true,
  ),
);
