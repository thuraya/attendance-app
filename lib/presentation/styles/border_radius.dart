import 'package:flutter/cupertino.dart';

var acceptBtnBorderRadius = BorderRadius.circular(32.0);
var denyLocationAccessBtnBorderRadius = BorderRadius.circular(32.0);
var checkInBorderRadius = BorderRadius.circular(16.0);
var checkOutBorderRadius = BorderRadius.circular(16.0);
var doneBtnBorderRadius = BorderRadius.circular(32.0);
var goToCheckInBtnBorderRadius = BorderRadius.circular(16.0);
var goToSalaryInBtnBorderRadius = BorderRadius.circular(16.0);
var locationGifBorderRadius = BorderRadius.all(Radius.circular(32.0));
var loginBtnBorderRadius = BorderRadius.circular(12.0);
var registerBtnBorderRadius = BorderRadius.circular(12.0);
var nextBtnBorderRadius = BorderRadius.circular(32.0);
var nextButtonBorderRadius = BorderRadius.circular(12.0);

var textFieldBorderRadius = BorderRadius.circular(12.0);
