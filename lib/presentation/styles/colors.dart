import 'package:flutter/material.dart';

class DesignColors {
  static const Color PRIMARY = DesignColors.BLUE;
  static const Color RED = const Color(0xFFD42C2C);
  static const Color ORANGE = const Color(0xFFE67B29);
  static const Color YELLOW = const Color(0xFFEABA14);
  static const Color ACCENT_GREEN = const Color(0xFF7BFED0);
  static const Color GREEN = const Color(0xFF5DD065);
  //static const Color BLUE = const Color(0xFF336ECF);
  static const Color BLUE = const Color(0xFF31DBF5);

  static const Color PURPLE = const Color(0xFF9440E9);

  static const Color WHITE = const Color(0xFFFFFFFF);
  static const Color LIGHT_RED = const Color(0xFFFFDCD1);
  static const Color LIGHT_GREY1 = const Color(0XFF3B3A41);
  static const Color LIGHT_GREY2 = const Color(0xFFA09EA9);
  static const Color DARK_COLOR1 = const Color(0xFF18171F);
  static const Color DARK_COLOR2 = const Color(0xFF1D1C23);
  static const Color BLACK = const Color(0xFF000000);

  static const Color BACKGROUND = DesignColors.DARK_COLOR1;
  static const Color TEXT_FIELD_FILL = DesignColors.DARK_COLOR2;
  static const Color FLIP = DesignColors.LIGHT_GREY1;
  static const Color LIGHT_TEXT = DesignColors.LIGHT_GREY2;
}
