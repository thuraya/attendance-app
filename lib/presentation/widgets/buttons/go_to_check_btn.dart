import 'package:flutter/material.dart';

import '../../styles/styles.dart';

class GoToCheckBtn extends StatelessWidget {
  final Function function;
  const GoToCheckBtn({Key key, this.function}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 2.0),
      padding: const EdgeInsets.symmetric(horizontal: 14.0),
      decoration: BoxDecoration(
        borderRadius: goToCheckInBtnBorderRadius,
        color: DesignColors.PRIMARY,
      ),
      child: TextButton(
        onPressed: () {
          function();
        },
        child: Text(
          'Go to check',
          style: TextStyle(
            color: DesignColors.WHITE,
          ),
        ),
      ),
    );
  }
}
