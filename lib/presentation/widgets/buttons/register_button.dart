import 'package:flutter/material.dart';

import '../../styles/styles.dart';

class RegisterButton extends StatelessWidget {
  const RegisterButton({Key key, @required this.onTap}) : super(key: key);
  final Function onTap;
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    double font18 = height * FONT18;
    return GestureDetector(
      onTap: onTap,
      child: Container(
        height: height * 0.07,
        width: width * 0.8,
        decoration: BoxDecoration(
          color: DesignColors.WHITE,
          borderRadius: registerBtnBorderRadius,
        ),
        child: Center(
          child: Text(
            'Sign Up',
            style: TextStyle(
                color: DesignColors.BLACK,
                fontSize: font18,
                fontWeight: FontWeight.w500),
          ),
        ),
      ),
    );
  }
}
