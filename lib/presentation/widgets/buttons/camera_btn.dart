import 'package:flutter/material.dart';

import '../../styles/styles.dart';

class CameraBtn extends StatelessWidget {
  const CameraBtn({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(8.0),
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        border: Border.all(
          color: DesignColors.BLACK,
          width: 1,
        ),
      ),
      child: Icon(
        Icons.camera_alt,
      ),
    );
  }
}
