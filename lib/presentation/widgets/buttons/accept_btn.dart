import 'package:flutter/material.dart';

import '../../styles/styles.dart';

class AcceptBtn extends StatelessWidget {
  final Function function;
  const AcceptBtn({Key key, this.function}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    double fontSize = height * FONT18;
    return Container(
      width: width * 0.62,
      height: height * 0.07,
      margin: const EdgeInsets.symmetric(vertical: 6.0),
      padding: const EdgeInsets.symmetric(horizontal: 14.0),
      decoration: BoxDecoration(
        borderRadius: acceptBtnBorderRadius,
        color: DesignColors.WHITE,
        border: Border.all(
          color: DesignColors.BACKGROUND,
        ),
      ),
      child: TextButton(
        onPressed: () {
          function();
          // locator<NavigationService>().navigateToNamed('/face_id');
        },
        child: Text(
          'OK',
          style: TextStyle(
            fontSize: fontSize,
            letterSpacing: 1.0,
            color: DesignColors.BACKGROUND,
          ),
        ),
      ),
    );
  }
}
