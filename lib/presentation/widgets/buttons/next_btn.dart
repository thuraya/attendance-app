import 'package:flutter/material.dart';

import '../../styles/styles.dart';

class NextBtn extends StatelessWidget {
  final Function function;
  const NextBtn({Key key, this.function}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 150,
      margin: const EdgeInsets.symmetric(vertical: 6.0),
      padding: const EdgeInsets.symmetric(horizontal: 14.0),
      decoration: BoxDecoration(
        borderRadius: nextBtnBorderRadius,
        color: DesignColors.PRIMARY,
      ),
      child: TextButton(
        onPressed: () {
          function();
        },
        child: Text(
          'Next',
          style: TextStyle(
            color: DesignColors.WHITE,
          ),
        ),
      ),
    );
  }
}
