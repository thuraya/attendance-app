import 'package:flutter/material.dart';

import '../../../services/services.dart';
import '../../styles/styles.dart';
import '../widgets.dart';

class DoneBtn extends StatelessWidget {
  final Function function;
  const DoneBtn({Key key, this.function}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 150,
      margin: const EdgeInsets.symmetric(vertical: 6.0),
      padding: const EdgeInsets.symmetric(horizontal: 14.0),
      decoration: BoxDecoration(
        borderRadius: doneBtnBorderRadius,
        color: DesignColors.PRIMARY,
      ),
      child: TextButton(
        onPressed: () async {
          locator<NavigationService>().navigateAndClearStack('/home');
          showToast('Thank You', context);
        },
        child: Text(
          'Done',
          style: TextStyle(
            color: DesignColors.WHITE,
          ),
        ),
      ),
    );
  }
}
