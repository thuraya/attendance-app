import 'package:flutter/material.dart';

import '../../../services/services.dart';
import '../../styles/styles.dart';

class CheckInBtn extends StatelessWidget {
  final Function function;
  const CheckInBtn({Key key, this.function}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 135,
      margin: const EdgeInsets.symmetric(vertical: 6.0),
      padding: const EdgeInsets.symmetric(horizontal: 14.0),
      decoration: BoxDecoration(
        borderRadius: checkInBorderRadius,
        color: DesignColors.PRIMARY,
      ),
      child: TextButton(
        onPressed: () {
          locator<NavigationService>().navigateToNamed('/location');
        },
        child: Text(
          'Check-In',
          style: TextStyle(
            color: DesignColors.WHITE,
          ),
        ),
      ),
    );
  }
}
