import 'package:flutter/material.dart';

import '../../../services/services.dart';
import '../../styles/styles.dart';

class DenyLocationAccessBtn extends StatelessWidget {
  final Function function;
  const DenyLocationAccessBtn({Key key, this.function}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    double fontSize = height * FONT16;
    return Container(
      width: width * 0.62,
      height: height * 0.07,
      //margin: const EdgeInsets.symmetric(vertical: 6.0),
      padding: const EdgeInsets.symmetric(horizontal: 14.0),
      decoration: BoxDecoration(
        borderRadius: denyLocationAccessBtnBorderRadius,
        color: DesignColors.RED,
        border: Border.all(
          color: DesignColors.RED,
        ),
      ),
      child: TextButton(
        onPressed: () {
          locator<NavigationService>().navigatePop();
        },
        child: Text(
          'Deny Location Access',
          style: TextStyle(
            fontSize: fontSize,
            // letterSpacing: 1.0,
            color: DesignColors.WHITE,
          ),
        ),
      ),
    );
  }
}
