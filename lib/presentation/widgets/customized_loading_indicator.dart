import 'package:flutter/material.dart';

import '../styles/styles.dart';

class ColoredCircularProgressIndicator extends StatelessWidget {
  const ColoredCircularProgressIndicator({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CircularProgressIndicator(
      color: DesignColors.PRIMARY,
      backgroundColor: DesignColors.WHITE,
    );
  }
}

class CustomizedLoadingIndicator extends StatelessWidget {
  final String message;
  const CustomizedLoadingIndicator({Key key, this.message}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double font16 = height * 0.0150;
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          message == null
              ? ColoredCircularProgressIndicator()
              : Column(
                  children: [
                    ColoredCircularProgressIndicator(),
                    SizedBox(
                      height: height * 0.0307,
                    ),
                    Center(
                      child: Text(
                        message,
                        style: TextStyle(
                          color: DesignColors.PRIMARY,
                          fontSize: font16,
                        ),
                      ),
                    )
                  ],
                )
        ],
      ),
    );
  }
}
