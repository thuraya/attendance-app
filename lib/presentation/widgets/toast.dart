import 'package:toast/toast.dart';

import '../styles/styles.dart';

showToast(String message, context) {
  Toast.show(
    message,
    context,
    duration: Toast.LENGTH_LONG,
    gravity: Toast.BOTTOM,
    textColor: DesignColors.BLACK,
    backgroundColor: DesignColors.WHITE,
  );
}
