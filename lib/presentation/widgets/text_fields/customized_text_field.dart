import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../styles/styles.dart';

class CustomizedTextField extends StatefulWidget {
  final TextEditingController controller;
  final FormState formState;
  final String hintText;
  final TextInputAction textInputAction;
  final Function(String val) validator;
  final IconData icon;
  const CustomizedTextField({
    Key key,
    @required this.hintText,
    @required this.textInputAction,
    @required this.validator,
    @required this.formState,
    @required this.controller,
    @required this.icon,
  }) : super(key: key);

  @override
  _CustomizedTextFieldState createState() => _CustomizedTextFieldState();
}

class _CustomizedTextFieldState extends State<CustomizedTextField> {
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    double font12 = height * FONT12;
    double font16 = height * FONT16;
    double hintSize = height * FONT16;
    double textSize = height * FONT18;
    double iconSize = height * FONT20;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      //mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          width: width * 0.8,
          decoration: BoxDecoration(
            color: DesignColors.BACKGROUND,
            borderRadius: textFieldBorderRadius,
          ),
          child: TextFormField(
            textAlignVertical: TextAlignVertical.center,
            style: TextStyle(
              color: DesignColors.WHITE,
              fontSize: textSize,
            ),
            cursorColor: DesignColors.WHITE,
            textInputAction: widget.textInputAction,
            controller: widget.controller,
            validator: widget.validator,
            onChanged: (val) {
              widget.formState.validate();
            },
            decoration: InputDecoration(
              fillColor: DesignColors.TEXT_FIELD_FILL,
              filled: true,
              hintText: widget.hintText,
              hintStyle: TextStyle(
                color: DesignColors.LIGHT_GREY1,
                fontSize: hintSize,
                fontStyle: FontStyle.normal,
              ),
              errorStyle: TextStyle(
                color: DesignColors.RED,
                fontSize: font12,
                height: height * 0.0016,
              ),
              contentPadding: EdgeInsets.symmetric(
                horizontal: font16,
              ),
              prefixIcon: widget.icon == null
                  ? SizedBox()
                  : Icon(
                      widget.icon,
                      color: DesignColors.LIGHT_GREY1,
                      size: iconSize,
                    ),
              enabledBorder:
                  textFieldOutlineInputBorder(DesignColors.LIGHT_GREY1),
              focusedBorder: textFieldOutlineInputBorder(
                  DesignColors.PRIMARY.withOpacity(0.5)),
              errorBorder:
                  textFieldOutlineInputBorder(DesignColors.LIGHT_GREY1),
              focusedErrorBorder:
                  textFieldOutlineInputBorder(DesignColors.LIGHT_GREY1),
            ),
          ),
        ),
        SizedBox(
          height: height * 0.03,
        ),
      ],
    );
  }

  OutlineInputBorder textFieldOutlineInputBorder(Color color) {
    return OutlineInputBorder(
      borderRadius: textFieldBorderRadius,
      borderSide: BorderSide(
        color: color,
      ),
    );
  }
}
