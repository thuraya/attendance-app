import 'package:attendance_logging_app/presentation/widgets/buttons/done_btn.dart';
import 'package:flutter/material.dart';

class CheckingDoneDialog extends StatelessWidget {
  const CheckingDoneDialog({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 12.0),
        height: 250,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              height: 16.0,
            ),
            Text(
              'Checked-in Successfully',
              style: TextStyle(
                fontSize: 16.0,
              ),
            ),
            Icon(
              Icons.thumb_up_alt,
              size: 50,
            ),
            Text(
              'In Office #12\nTime: 8:10 A.M\n16-09-2021\nYou\'re 10 minutes late',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 14.0,
              ),
            ),
            DoneBtn(),
          ],
        ),
      ),
    );
  }
}
